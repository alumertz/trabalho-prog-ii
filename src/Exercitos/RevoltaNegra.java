package Exercitos;

import java.util.ArrayList;
import Unidades.*;

/**
 *Classe que estrutura o exército inimigo da batalha de Fanfa.
 * Contém quantidade de tropas e métodos para utilização.
 * @author Augusto Garcia e Éric Peralta.
 */
public class RevoltaNegra {    
    private static int quntGen = 0;
    private static int quntCav = 0;
    private static int quntPad = 0;
    private static int quntLan = 50;
    private static int quntInf = 0;

    /**Método construtor de ArroioGrande*/
    RevoltaNegra(){}
    
    /**Método para retorno da quantidade de generais.
     * @return int - quantidade de generais.
     */
    public static int getQuntGen() {
        return quntGen;
    }

    /**Método para retorno da quantidade de cavaleiros.
     * @return int - quantidade de cavaleiros.
     */
    public static int getQuntCav() {
        return quntCav;
    }

    /**Método para retorno da quantidade de padres.
     * @return int - quantidade de padres.
     */
    public static int getQuntPad() {
        return quntPad;
    }

     /**Método para retorno da quantidade de lanceiros negros.
     * @return int - quantidade de lanceiros negros.
     */
    public static int getQuntLan() {
        return quntLan;
    }

    /**Método para retorno da quantidade de infantarias.
     * @return int - quantidade de infantarias.
     */
    public static int getQuntInf() {
        return quntInf;
    }
}
