/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Unidades;

import java.util.ArrayList;
import java.util.Random;

/**
 *Classe abstrata de unidades(lanceiro, padre, general, infantaria e cavaleiro)
 * @author Augusto Garcia e Éric Peralta
 */
public abstract class Unidade {

    /**
     *Valor de ataque.
     */
    protected int ataque;

    /**
     *Valor de vida.
     */
    protected int vida;

    /**
     *Variável de Item.
     */
    protected Item arma;

    /**
     *Variável de sorteio.
     */
    protected Random r = new Random();
    
    /**
     *Método abstrato de ataque especial.
     * @param inimigo int - Inimigo a ser atacado.
     */
    public abstract void ataqueEspecial(Unidade inimigo);

    /**
     *Método abstrato de definir vida e ataque.
     */
    public abstract void setTudo();

    /**
     *Método de definir ataque.
     * @param ataque int - valor do ataque.
     */
    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    /**
     *Método de definir a vida.
     * @param vida int valor da vida.
     */
    public void setVida(int vida) {
        this.vida = vida;
    }
    
    /**
     *Métodoo que retorna ataque
     * @return int - retorna ataque.
     */
    public int getAtaque(){
        if(this.arma.getDefesa()>0){
            return (this.ataque + this.arma.getAtaque());
        }
        else{
            return this.ataque;
        }
    }
    
    /**
     *Método que retorna vida.
     * @return int - retorna vida.
     */
    public int getVida(){
        return this.vida;
    }
    
    /**
     *Método que efetua o dano. 
     * @param atk int - valor do dano.
     */
    public void sofreDano(int atk){
        if(this.arma.getDefesa()>0){
            this.arma.setDefesa(this.arma.getDefesa()-atk);
        }
        
        else{
            this.vida -= atk;
        }
    }
    
    /**
     * Método que retorba o ataque base.
     * @return int - retorna ataque base.
     */
    public int getAtaqueBase(){
        return this.ataque;
    }
    
    /**
     *Método de ataque especial para padres.
     * @param aliados Arraylist - lista de aliados.
     */
    public void ataqueEspecial(ArrayList<Unidade> aliados){// para o padre
       int u = r.nextInt(aliados.size());
       int v = aliados.get(u).getVida();
       
       if(this.arma.getDefesa()>0){
           aliados.get(u).setVida(v+(v/5));
       }
   }
}
