/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetogame;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import java.util.ArrayList;
import Exercitos.*;
import Unidades.*;
/**
 * FXML Controller class
 *Classe controller da tela de Escolha Batalha.
 * @author Augusto Garcua e Éric Peralta
 */
public class EscolhaBatalhaController implements Initializable {

    private static ArrayList<Unidade> inimigos;
    private static int p;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inimigos = new ArrayList<Unidade>();
    }
    @FXML
    private void iniciaArroioGrande(){
       for(int i=0;i<ArroioGrande.getQuntCav();i++){
            inimigos.add(new Cavaleiro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<ArroioGrande.getQuntGen();i++){
            inimigos.add(new General());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<ArroioGrande.getQuntLan();i++){
            inimigos.add(new LanceiroNegro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<ArroioGrande.getQuntInf();i++){
            inimigos.add(new Infantaria());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<ArroioGrande.getQuntPad();i++){
            inimigos.add(new Padre());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        p=1;
        ProjetoGame.trocaTela("batalhatela.fxml");
    }
    
    @FXML
    private void iniciaFanfa(){
        for(int i=0;i<Fanfa.getQuntCav();i++){
            inimigos.add(new Cavaleiro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<Fanfa.getQuntGen();i++){
            inimigos.add(new General());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<Fanfa.getQuntLan();i++){
            inimigos.add(new LanceiroNegro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<Fanfa.getQuntInf();i++){
            inimigos.add(new Infantaria());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<Fanfa.getQuntPad();i++){
            inimigos.add(new Padre());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        p=2;
        ProjetoGame.trocaTela("batalhatela.fxml");
    }
    
    @FXML
    private void iniciaPonteDaAzenha(){
        for(int i=0;i<PonteDaAzenha.getQuntCav();i++){
            inimigos.add(new Cavaleiro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<PonteDaAzenha.getQuntGen();i++){
            inimigos.add(new General());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<PonteDaAzenha.getQuntLan();i++){
            inimigos.add(new LanceiroNegro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<PonteDaAzenha.getQuntInf();i++){
            inimigos.add(new Infantaria());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        for(int i=0;i<PonteDaAzenha.getQuntPad();i++){
            inimigos.add(new Padre());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        
        p=3;
        ProjetoGame.trocaTela("batalhatela.fxml");
    }
    
    @FXML
    private void iniciaRevoltaNegra(){        
        for(int i=0;i<RevoltaNegra.getQuntLan();i++){
            inimigos.add(new LanceiroNegro());
            inimigos.get(inimigos.size()-1).setTudo();
        }
        p=4;
        ProjetoGame.trocaTela("batalhatela.fxml");
    }

    /**
     *
     * @return
     */
    public static ArrayList<Unidade> getInimigos() {
        return inimigos;
    }
    
    /**
     *
     * @return
     */
    public static int getP(){
        return p;
    }

    /**
     *
     */
    public void VoltarButton(){
            ProjetoGame.trocaTela("TelaInicial.fxml");
    }
}
