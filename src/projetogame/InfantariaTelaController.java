
package projetogame;

import Exercitos.Dados;
import Exercitos.Usuario;
import Unidades.Infantaria;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *Classe controller da tela de Infantaria.
 * @author Augusto Garcia e Éric Peralta
 */
public class InfantariaTelaController implements Initializable {

    @FXML private Label dindin;
    @FXML private TextField qunt;
    @FXML private Label aviso;
    @FXML private Label cust;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dindin.setText("R$ " + Usuario.getDinheiro());
    }
    @FXML
    private void calcust(){
        cust.setText(String.valueOf(Dados.getInfCust()*Integer.parseInt(qunt.getText())));
    }
    
    @FXML
    private void VoltarButton(){
        int val = Integer.parseInt(qunt.getText());
        if((Dados.getInfCust()*val) <= Usuario.getDinheiro()){
            Usuario.diminui(val*Dados.getInfCust());
            for(int i=0;i<val;i++){
                Usuario.addInf();
            }
            ProjetoGame.trocaTela("TelaInicial.fxml");
        }
        
       else{
            aviso.setText("Não a dinheiro suficiente");
        }
    }
    
    @FXML
    private void maisVida(){
        if(Usuario.getDinheiro()>=10){
            Dados.setInfVid(Dados.getInfVid() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    @FXML
    private void maisAtaque(){
        if(Usuario.getDinheiro()>=10){
            Dados.setInfAtk(Dados.getInfAtk() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    private void atualizaDinheiro(){
        dindin.setText("R$ "+ Usuario.getDinheiro());
    }
}