
package projetogame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *Classe Main.
 * @author Augusto Garcia e Éric Peralta
 */
public class ProjetoGame extends Application {
    private static Stage stage;
    
    /**
     *Método que retorna o estágio que esta a interface.
     * @return Stage - retorna o estágio.
     */
    public static Stage getStage(){
        return stage;
    }
    
    /**
     *Método que troca a tela da interface.
     * @param tela String - endereço da nova tela.
     */
    public static void trocaTela(String tela){
        Parent root = null;
        
        try{
            root = FXMLLoader.load(ProjetoGame.class.getResource(tela));
        }
        catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("TelaInicial.fxml"));
        Scene scene = new Scene(root);
        
        ProjetoGame.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
